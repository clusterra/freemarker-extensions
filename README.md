# Clusterra freemarker extensions

[![Apache License](https://img.shields.io/badge/Licence-Apache%202.0-blue.svg?style=flat-square)](http://www.apache.org/licenses/LICENSE-2.0)    
[![The Central Repository](https://img.shields.io/badge/Artifacts-The%20Central%20Repository-orange.svg?style=flat-square)][iam-artifacts-mvn]
[![Sonatype OSS](https://img.shields.io/badge/Artifacts-Sonatype%20OSS-orange.svg?style=flat-square)][iam-artifacts-sonatype]     
[![Build Status](https://drone.io/bitbucket.org/clusterra/freemarker-extensions/status.png)](https://drone.io/bitbucket.org/clusterra/freemarker-extensions/latest)

### freemarker-renderer module

Used to render HTML for emails or PDF.

* supports using i18n messages in template, via msg('message.some.property')
* customized configuration

### set-up

Project uses a [Gradle][] build system.     
In the instructions below, `./gradlew` is invoked from the root of the source tree and serves as a cross-platform, self-contained bootstrap mechanism for the build.    
   
The gradle tasks of particular interest:   
`gradlew build`   
`gradlew idea` - generate IntelliJ IDEA project modules   
`gradlew intTest` - execute integration tests, will require postgresql db instance.   


[Gradle]: http://gradle.org
[iam-artifacts-mvn]: http://search.maven.org/#search%7Cga%7C1%7Ca%3A%22clusterra-freemarker-renderer%22
[iam-artifacts-sonatype]: https://oss.sonatype.org/#nexus-search;gav~com.clusterra~clusterra-freemarker-renderer~~~~kw,versionexpand