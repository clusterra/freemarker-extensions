/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.freemarker.renderer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Denis Kuchugurov
 * on 09/06/15 07:27.
 */
@ContextConfiguration(
        value = {
                "classpath*:META-INF/spring/*.xml"
        },
        initializers = ConfigFileApplicationContextInitializer.class
)
//@WebAppConfiguration
public class FreemarkerTemplateRendererTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private FreemarkerTemplateRenderer freemarkerTemplateRenderer;

    @Test
    public void test_render() throws Exception {

        //can be dropped if @WebAppConfiguration used
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setServerName("my-host.com");
        request.setServerPort(-1);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        Map<String, Object> variables = new HashMap<>();
        variables.put("activationToken", "ASD");
        freemarkerTemplateRenderer.render("test.ftl", variables);

    }
}
