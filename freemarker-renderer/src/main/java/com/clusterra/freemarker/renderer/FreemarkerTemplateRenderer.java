/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.freemarker.renderer;

import com.clusterra.freemarker.renderer.resolvehost.HostResolver;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Denis Kuchugurov
 * on 08/04/15 18:52.
 */
@Component
public class FreemarkerTemplateRenderer {

    @Autowired
    private Configuration configuration;

    @Autowired
    private MessageResolverMethod messageResolverMethod;

    @Autowired
    private HostResolver hostResolver;


    public String render(String templateName, Map<String, Object> variables) {
        try {
            variables.put("msg", messageResolverMethod);
            variables.put("host", hostResolver.resolveHost());
            Template template = configuration.getTemplate(templateName);
            return FreeMarkerTemplateUtils.processTemplateIntoString(template, variables);
        } catch (IOException | TemplateException e) {
            throw new RuntimeException(e);
        }
    }

}
