/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.freemarker.renderer;

import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Denis Kuchugurov
 * on 03/04/15 14:02.
 */
@Component
public class MessageResolverMethod implements TemplateMethodModelEx {

    @Autowired
    private MessageSource messageSource;

    @Override
    public Object exec(List arguments) throws TemplateModelException {
        if (arguments.size() != 1) {
            throw new TemplateModelException("Wrong number of arguments");
        }
        Object argument = arguments.get(0);

        if (argument == null || !(argument instanceof SimpleScalar)) {
            throw new TemplateModelException("Invalid argument value '" + argument + "'");
        }
        SimpleScalar code = (SimpleScalar) argument;
        return messageSource.getMessage(code.getAsString(), null, LocaleContextHolder.getLocale());
    }
}
